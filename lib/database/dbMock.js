var Message = require('../model/message');
var ids = 1;
var messagesById = {};
var messagesByKey = {};

function getKey(message) {
    return message.getProject() + message.getKey() + message.getLanguage() + message.getCountry();
}

var db = {
    getType: function() {
        return 'mock';
    },
    init: function() {
        ids = 1;
        messagesById = {};
        messagesByKey = {};
    },
    insert: function(message) {
        message.id = ids++;
        this.save(message);
    },
    save: function(message) {
        var messageJSON = message.toJSON();
        var key = getKey(message);

        messagesById[message.getId()] = messageJSON;
        messagesByKey[key] = messageJSON;
    },
    get: function(id) {
        var json = messagesById[id];
        if (json) {
            return Message.fromJSON(json);
        }
        return null;
    },
    find: function(project, key, language, country) {
        var messages = [];
        var json = messagesByKey[project + key + language + country];
        if (json) {
            messages.push(Message.fromJSON(json));
        }
        return messages;
    }
};

module.exports = db;