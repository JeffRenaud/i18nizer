var type = {
    MONGO: 'mongo',
    MOCK: 'mock'
};

var dbType = type.MONGO;

var dbConfig = {
    typeEnum: type,
    getType: function() {
        return dbType;
    },
    setMongo: function() {
        dbType = type.MONGO;
    },
    setMock: function() {
        dbType = type.MOCK;
    }
};

module.exports = dbConfig;