var mongoskin = require('mongoskin');
var Message = require('../model/message');
var Q = require('q');
var db;

var mongo = {
    setDatabase: function (database) {
        db = mongoskin.db('localhost:27017/' + database, {
            safe: true
        });
    },
    getType: function () {
        return 'mongo';
    },
    init: function () {
        db.collection('messages').drop();
    },
    insert: function (message) {
        var deferred = Q.defer();
        db.collection('messages').insert(message.toJSON(), function (err, result) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(new Message(result[0]));
            }
        });
        return deferred.promise;
    },
    save: function (message) {
        var deferred = Q.defer();
        var criteria = {
            '_id': message.getId()
        };
        db.collection('messages').update(criteria, message.toJSON(), function (err, result) {
            if (err) {
                deferred.reject(err);
            } else {
                if (result === 0) {
                    deferred.reject(new Error('No message found'));
                } else {
                    deferred.resolve(message);
                }
            }
        });
        return deferred.promise;
    },
    get: function (id) {
        var deferred = Q.defer();
        db.collection('messages').findById(id, function (err, result) {
            if (err) {
                deferred.reject(err);
            } else {
                if (result) {
                    deferred.resolve(new Message(result));
                } else {
                    deferred.resolve(null);
                }
            }
        });
        return deferred.promise;
    },
    find: function (project, key, language, country) {
        var deferred = Q.defer(),
            json = {};

        if (project) {
            json.project = project;
        }
        if (key) {
            json.key = key;
        }
        if (language) {
            json.language = language;
        }
        if (country) {
            json.country = country;
        }
        
        db.collection('messages').find(json).toArray(function (err, result) {
            if (err) {
                deferred.reject(err);
            } else {
                var messages = [];
                for (var i = 0; i < result.length; i++) {
                    messages.push(new Message(result[i]));
                }
                deferred.resolve(messages);
            }
        });

        return deferred.promise;
    },
    getAllProjects: function () {
        var deferred = Q.defer();

        db.collection('messages').distinct('project', {}, function (err, result) {
            if (err) {
                deferred.reject(err);
            } else {
                deferred.resolve(result);
            }
        });

        return deferred.promise;
    }
};

module.exports = mongo;