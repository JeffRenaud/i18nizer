var dbConfig = require('./dbConfig');

var dbFactory = {
    getDatabase: function() {
        var db = require('./dbMongo');
        if (dbConfig.getType() === dbConfig.typeEnum.MOCK) {
            db.setDatabase('i18nmock');
        }
        else {
            db.setDatabase('i18n');
        }
        return db; 
    }
};

module.exports = dbFactory;