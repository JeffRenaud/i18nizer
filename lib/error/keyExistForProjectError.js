var CustomError = require('./customError');

var KeyExistForProjectError = function () {
    CustomError.call(this, 'KeyExistForProjectError', '0x0001', 'A key already exist for the given project');
};

KeyExistForProjectError.prototype = Object.create(CustomError.prototype);

module.exports = KeyExistForProjectError;