var CustomError = require('./customError');

var MessageNotFoundError = function () {
    CustomError.call(this, 'MessageNotFoundError', '0x0005', 'Message not found');
};

MessageNotFoundError.prototype = Object.create(CustomError.prototype);

module.exports = MessageNotFoundError;