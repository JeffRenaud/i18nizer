var CustomError = require('./customError');

var KeyExistForLocaleError = function () {
    CustomError.call(this, 'KeyExistForLocaleError', '0x0003', 'A key already exist for the locale');
};

KeyExistForLocaleError.prototype = Object.create(CustomError.prototype);

module.exports = KeyExistForLocaleError;