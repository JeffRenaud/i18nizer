var CustomError = function (name, code, message) {
    this.name = name;
    this.code = code;
    this.message = message;
    Error.captureStackTrace(this, this.constructor);
};

CustomError.prototype = Object.create(Error.prototype);

CustomError.prototype.constructor = CustomError;

CustomError.prototype.toJSON = function() {
  return {
      name: this.name,
      code: this.code,
      message: this.message
  };
};

module.exports = CustomError;