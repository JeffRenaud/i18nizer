var CustomError = require('./customError');

var KeyNotFoundError = function () {
    CustomError.call(this, 'KeyNotFoundError', '0x0002', 'A key was not found');
};

KeyNotFoundError.prototype = Object.create(CustomError.prototype);

module.exports = KeyNotFoundError;