var CustomError = require('./customError');

var KeyForLanguageIsMissingError = function () {
    CustomError.call(this, 'KeyForLanguageIsMissingError', '0x0004', 'A key for the language has to be created first');
};

KeyForLanguageIsMissingError.prototype = Object.create(CustomError.prototype);

module.exports = KeyForLanguageIsMissingError;