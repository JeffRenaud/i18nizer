var Message = function (json) {
    this._id = json._id || null;
    this.project = json.project;
    this.key = json.key;
    this.value = json.value || null;
    this.language = json.language || 'ALL';
    this.country = json.country || 'ALL';
    this.parentId = json.parentId || null;
    this.parentMessage = null;
};

Message.prototype.getId = function () {
    return this._id;
};

Message.prototype.getKey = function () {
    return this.key;
};

Message.prototype.setValue = function (value) {
    this.value = value;
};

Message.prototype.getValue = function () {
    if (this.value) {
        return this.value;
    } else if (this.hasParent()) {
        return this.parentMessage.getValue();
    } else {
        return '[' + this.key + ']';
    }
};

Message.prototype.getProject = function () {
    return this.project;
};

Message.prototype.getLanguage = function () {
    return this.language;
};

Message.prototype.getCountry = function () {
    return this.country;
};

Message.prototype.setParentMessage = function (message) {
    this.parentMessage = message;
};

Message.prototype.getParentMessage = function () {
    return this.parentMessage;
};

Message.prototype.getParentId = function () {
    return this.parentId;
};

Message.prototype.hasParent = function () {
    return this.parentId != null;
};

Message.prototype.toJSON = function () {
    var json = {
        project: this.project,
        key: this.key,
        value: this.value,
        language: this.language,
        country: this.country,
        parentId: this.parentId
    };

    if (this._id) {
        json._id = this._id;
    }

    return json;
};

Message.fromJSON = function (json) {
    var obj = JSON.parse(json);

    return new Message({
        _id: obj._id,
        project: obj.project,
        key: obj.key,
        value: obj.value,
        language: obj.language,
        country: obj.country,
        parentId: obj.parentId
    });
};

module.exports = Message;