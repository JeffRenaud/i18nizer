var i18nService = require('../service/i18nService');
var languageCode = require('../model/languageCode');
var countryCode = require('../model/countryCode');

var routes = {
    getProjects: function (req, res) {
        i18nService.getProjects().then(function (projects) {
            res.send(projects);
        });
    },
    getAllMessages: function (req, res) {
        i18nService.findMessages(req.params.project).then(function (messages) {
            var json = [];
            for (var i = 0; i < messages.length; i++) {
                json.push(messages[i].toJSON());
            }
            res.send(json);
        }).fail(function (err) {
            res.status(403);
            res.send(err.toJSON());
        });
    },
    getKeys: function (req, res) {
        i18nService.findMessages(req.params.project, null, languageCode.ALL, countryCode.ALL).then(function (messages) {
            var json = [];
            for (var i = 0; i < messages.length; i++) {
                json.push(messages[i].toJSON());
            }
            res.send(json);
        }).fail(function (err) {
            res.status(403);
            res.send(err.toJSON());
        });
    },
    addKey: function (req, res) {
        i18nService.addKey(req.params.project, req.params.key).then(function (message) {
            res.send(message.toJSON());
        }).fail(function (err) {
            res.status(403);
            res.send(err.toJSON());
        });
    },
    addLocale: function (req, res) {
        i18nService.addLocale(req.params.project, req.params.key, req.params.language, req.params.country || countryCode.ALL).then(function (message) {
            res.send(message.toJSON());
        }).fail(function (err) {
            res.status(403);
            res.send(err.toJSON());
        });
    }
};

module.exports = routes;