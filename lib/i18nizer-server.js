'use strict';

var express = require('express');
var app = express();
var routes = require('./route/routes');

/** API **

POST    /i18n/projects/:project/keys/:key
POST    /i18n/projects/:project/keys/:key/value/:value
POST    /i18n/projects/:project/keys/:key/locale/en
POST    /i18n/projects/:project/keys/:key/locale/en-us
*POST   /i18n/projects/:project/keys/:key/locale/en/value/:value
*POST   /i18n/projects/:project/keys/:key/locale/en-us/value/:value

PUT     /i18n/projects/:project/message/:id/value/:value

GET     /i18n/projects/:project
GET     /i18n/projects/:project/export/json
GET     /i18n/projects/:project/export/gadget

**********/

app.get('/', function (req, res) {
    // Read the .i18nizerconfig, if not found, ask the user to set some parameters upfront before creating it
    res.send('hi from i18nizer-server v2: ' + process.cwd());
});

app.get('/i18n/projects', routes.getProjects);
app.get('/i18n/projects/:project', routes.getAllMessages);
app.get('/i18n/projects/:project/keys', routes.getKeys);
app.post('/i18n/projects/:project/keys/:key', routes.addKey);
app.post('/i18n/projects/:project/keys/:key/language/:language', routes.addLocale);
app.post('/i18n/projects/:project/keys/:key/language/:language/country/:country', routes.addLocale);

app.listen(8118);
console.log('Listening on port 8118');

module.exports = app;