var db = require('../database/dbFactory').getDatabase();
var Q = require('q');

var Message = require('../model/message');
var languageCode = require('../model/languageCode');
var countryCode = require('../model/countryCode');

var KeyExistForProjectError = require('../error/keyExistForProjectError');
var KeyNotFoundError = require('../../lib/error/keyNotFoundError');
var KeyExistForLocaleError = require('../../lib/error/keyExistForLocaleError');
var KeyForLanguageIsMissingError = require('../../lib/error/keyForLanguageIsMissingError');
var MessageNotFoundError = require('../../lib/error/messageNotFoundError');

function getMessage(project, key, language, country) {
    var deferred = Q.defer();
    db.find(project, key, language, country).then(function (messages) {
        if (messages.length) {
            deferred.resolve(messages[0]);
        } else {
            deferred.resolve(null);
        }
    });
    return deferred.promise;
}

function getParentMessage(project, key, language, country) {
    var deferred = Q.defer();

    if (language !== languageCode.ALL) {
        if (country !== countryCode.ALL) {
            db.find(project, key, language, countryCode.ALL).then(function (messages) {
                if (messages.length) {
                    getParentMessage(project, key, language, countryCode.ALL).then(function(parentMessage) {
                        messages[0].setParentMessage(parentMessage);
                        deferred.resolve(messages[0]);
                    });
                } else {
                    deferred.resolve(null);
                }
            });
        } else {
            db.find(project, key, languageCode.ALL, countryCode.ALL).then(function (messages) {
                if (messages.length) {
                    deferred.resolve(messages[0]);
                } else {
                    deferred.resolve(null);
                }
            });
        }
    } else {
        deferred.resolve(null);
    }

    return deferred.promise;
}

var i18nizerService = {
    init: function () {
        db.init();
    },
    addKey: function (project, key, value) {
        var deferred = Q.defer();

        getMessage(project, key, languageCode.ALL, countryCode.ALL).then(function (keyMessage) {
            if (keyMessage) {
                deferred.reject(new KeyExistForProjectError());
            } else {
                var message = new Message({
                    project: project,
                    key: key,
                    value: value
                });
    
                db.insert(message).then(function (message) {
                    deferred.resolve(message);
                });
            }
        });

        return deferred.promise;
    },
    addLocale: function (project, key, language, country) {
        var deferred = Q.defer();
        
        getParentMessage(project, key, language, country).then(function (parentMessage) {
            if (!parentMessage) {
                if (country === countryCode.ALL) {
                    deferred.reject(new KeyNotFoundError());
                } else {
                    deferred.reject(new KeyForLanguageIsMissingError());
                }
            } else {
                getMessage(project, key, language, country).then(function (message) {
                    if (message) {
                        deferred.reject(new KeyExistForLocaleError());
                    } else {
                        var newMessage = new Message({
                            project: project,
                            key: key,
                            language: language,
                            country: country,
                            parentId: parentMessage.getId()
                        });

                        db.insert(newMessage).then(function (message) {
                            message.setParentMessage(parentMessage);
                            deferred.resolve(message);
                        });
                    }
                });
            }
        });

        return deferred.promise;
    },
    findMessages: function (project, key, language, country) {
        var deferred = Q.defer();
        
        db.find(project, key, language, country).then(function(messages) {
            if (!messages.length) {
                deferred.reject(new MessageNotFoundError());
            } else {
                var promises = [],
                    i;
                for (i = 0; i < messages.length; i++) {
                    var message = messages[i];
                    if (message.hasParent()) {
                        promises.push(getParentMessage(message.getProject(), message.getKey(), message.getLanguage(), message.getCountry()));
                    }
                }
                
                Q.all(promises).then(function(parentMessages) {
                    for (i = 0; i < messages.length; i++) {
                        var message = messages[i];
                        if (message.hasParent()) {
                            message.setParentMessage(parentMessages[i]);
                        }
                    }
                    deferred.resolve(messages);
                });
            }
        });
        
        return deferred.promise;
    },
    updateMessage: function (message) {
        var deferred = Q.defer();
        
        db.save(message).then(function(message) {
            deferred.resolve(message);
        });
        
        return deferred.promise;
    },
    getProjects: function() {
        var deferred = Q.defer();
        
        db.getAllProjects().then(function(projects) {
            deferred.resolve(projects);
        });
        
        return deferred.promise;
    }
};

module.exports = i18nizerService;