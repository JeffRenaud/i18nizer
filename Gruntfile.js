/*global module, require*/
module.exports = function (grunt) {
    'use strict';

    // Show elapsed time at the end
    require('time-grunt')(grunt);
    // Load all grunt tasks
    require('load-grunt-tasks')(grunt);
    
    // Project configuration.
    grunt.initConfig({
        jasmine_node: {
            coverage: {

            },
            options: {
                forceExit: true,
                match: '.',
                matchall: false,
                extensions: 'js',
                specNameMatcher: 'spec',
                junitreport: {
                    report: true,
                    savePath: "./build/reports/jasmine/",
                    useDotNotation: true,
                    consolidate: true
                }
            }
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            gruntfile: {
                src: 'Gruntfile.js'
            },
            lib: {
                src: ['lib/**/*.js']
            },
            test: {
                src: ['test/**/*.js']
            }
        },
        watch: {
            gruntfile: {
                files: '<%= jshint.gruntfile.src %>',
                tasks: ['jshint:gruntfile']
            },
            lib: {
                files: '<%= jshint.lib.src %>',
                tasks: ['jshint:lib', 'nodeunit']
            },
            test: {
                files: '<%= jshint.test.src %>',
                tasks: ['jshint:test', 'nodeunit']
            }
        },
        express: {
            options: {
                port: 8118,
                hostname: '*',
                server: 'lib/i18nizer-server'
                //serverreload: true
            }
        }
    });

    // Default task.
    grunt.registerTask('default', ['jshint', 'jasmine_node']);
    grunt.registerTask('test', 'jasmine_node');
    grunt.registerTask('serve', 'express');
};