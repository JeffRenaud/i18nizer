# i18nizer-server [![Build Status](https://secure.travis-ci.org//i18nizer-server.png?branch=master)](http://travis-ci.org//i18nizer-server)

REST api to configure google gadget i18n

node --debug-brk /usr/local/lib/node_modules/jasmine-node/lib/jasmine-node/cli.js test/shared/i18nService-spec.js 

## Getting Started
Install the module with: `npm install i18nizer-server`

```javascript
var i18nizer-server = require('i18nizer-server');
i18nizer-server.awesome(); // "awesome"
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2013 Jean-Francois Renaud. Licensed under the MIT license.
