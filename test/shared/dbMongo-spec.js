/* global beforeEach, describe, it, expect */
var db = require('../../lib/database/dbMongo');
db.setDatabase('i81nmock');
var Q = require('q');
var Message = require('../../lib/model/message');
var languageCode = require('../../lib/model/languageCode');
var countryCode = require('../../lib/model/countryCode');

describe('dbMongo', function () {
    var project = 'project',
        key = 'key';

    function createMessage(language, country, anotherKey) {
        return new Message({
            project: project,
            key: anotherKey || key,
            language: language || languageCode.ALL,
            country: country || countryCode.ALL
        });
    }

    beforeEach(function () {
        db.init();
    });

    describe('insert', function () {
        it('should insert a message and return the newly saved message with an id', function (done) {
            db.insert(createMessage()).then(function (message) {
                expect(message).toBeDefined();
                expect(message.getId()).toBeDefined();
                done();
            });
        });
    });

    describe('get', function () {
        it('should return null when no message has been found', function (done) {
            db.get('1234').then(function (message) {
                expect(message).toBe(null);
                done();
            });
        });

        it('should get a message by his id', function (done) {
            db.insert(createMessage()).then(function (messageCreated) {
                db.get(messageCreated.getId()).then(function (message) {
                    expect(message.getId()).toEqual(messageCreated.getId());
                    done();
                });
            });
        });
    });

    describe('save', function () {
        it('should update a message and return the saved message', function (done) {
            db.insert(createMessage()).then(function (message) {
                message.setValue('updated');
                db.save(message).then(function (updatedMessage) {
                    expect(updatedMessage.getId()).toEqual(message.getId());
                    expect(updatedMessage.getValue()).toBe('updated');
                    done();
                });
            });
        });
    });

    describe('find', function () {
        it('should return an empty arry when no message has been found', function (done) {
            db.find(project, key, languageCode.ALL, countryCode.ALL).then(function (messages) {
                expect(messages.length).toBe(0);
                done();
            });
        });

        it('should return the messages when they are found - one message', function (done) {
            db.insert(createMessage()).then(function (message) {
                db.find(message.getProject(), message.getKey(), message.getLanguage(), message.getCountry()).then(function (messages) {
                    expect(messages.length).toBe(1);
                    done();
                });
            });
        });

        it('should return the messages when they are found - multiple messages', function (done) {
            var promise1 = db.insert(createMessage());
            var promise2 = db.insert(createMessage(languageCode.EN, countryCode.ALL));

            Q.all([promise1, promise2]).then(function () {
                db.find(project, key, languageCode.ALL, countryCode.ALL).then(function (messages) {
                    expect(messages.length).toBe(1);
                    done();
                });
            });
        });

        it('should be able to find all messages for a key', function (done) {
            var promise1 = db.insert(createMessage());
            var promise2 = db.insert(createMessage(languageCode.EN, countryCode.ALL));
            var promise3 = db.insert(createMessage(languageCode.ALL, countryCode.ALL, 'key2'));

            Q.all([promise1, promise2, promise3]).then(function () {
                db.find(project, key).then(function (messages) {
                    expect(messages.length).toBe(2);
                    expect(messages[0].getKey()).toBe(key);
                    expect(messages[1].getKey()).toBe(key);
                    done();
                });
            });
        });

        it('should be able to find all messages for a project', function (done) {
            var promise1 = db.insert(createMessage());
            var promise2 = db.insert(createMessage(languageCode.EN, countryCode.ALL));
            var promise3 = db.insert(createMessage(languageCode.ALL, countryCode.ALL, 'key2'));

            Q.all([promise1, promise2, promise3]).then(function () {
                db.find(project).then(function (messages) {
                    expect(messages.length).toBe(3);
                    done();
                });
            });
        });

        it('should be able to find all messages for a language', function (done) {
            var promise1 = db.insert(createMessage());
            var promise2 = db.insert(createMessage(languageCode.EN, countryCode.ALL));
            var promise3 = db.insert(createMessage(languageCode.EN, countryCode.CA));
            var promise4 = db.insert(createMessage(languageCode.ALL, countryCode.ALL, 'key2'));

            Q.all([promise1, promise2, promise3, promise4]).then(function () {
                db.find(project, key, languageCode.EN).then(function (messages) {
                    expect(messages.length).toBe(2);
                    expect(messages[0].getLanguage()).toBe(languageCode.EN);
                    expect(messages[1].getLanguage()).toBe(languageCode.EN);
                    done();
                });
            });
        });
    });

    describe('getAllProjects', function () {
        it('should return an empty arry when no project exist', function (done) {
            db.getAllProjects().then(function (projects) {
                expect(projects.length).toBe(0);
                done();
            });
        });

        it('should return the list of projects', function (done) {
            var promise1 = db.insert(new Message({
                project: 'project1',
                key: key
            }));
            var promise2 = db.insert(new Message({
                project: 'project2',
                key: key
            }));
            var promise3 = db.insert(new Message({
                project: 'project3',
                key: key
            }));
            var promise4 = db.insert(new Message({
                project: 'project4',
                key: key
            }));

            Q.all([promise1, promise2, promise3, promise4]).then(function () {
                db.getAllProjects().then(function (projects) {
                    expect(projects.length).toBe(4);
                    done();
                });
            });
        });
    });
});