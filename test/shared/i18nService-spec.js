/* global describe, it, expect, beforeEach */
require('../../lib/database/dbConfig').setMock();
var Q = require('q');

var i18nService = require('../../lib/service/i18nService');

var languageCode = require('../../lib/model/languageCode');
var countryCode = require('../../lib/model/countryCode');

var KeyExistForProjectError = require('../../lib/error/keyExistForProjectError');
var KeyNotFoundError = require('../../lib/error/keyNotFoundError');
var KeyExistForLocaleError = require('../../lib/error/keyExistForLocaleError');
var KeyForLanguageIsMissingError = require('../../lib/error/keyForLanguageIsMissingError');
var MessageNotFoundError = require('../../lib/error/messageNotFoundError');

describe('i18nService', function () {
    var project = 'project';
    var key = 'key';
    var value = 'value';

    beforeEach(function () {
        i18nService.init();
    });

    describe('addKey', function () {
        it('should add a default value (key in bracket) if not specified', function (done) {
            i18nService.addKey(project, key).then(function (message) {
                expect(message.getProject()).toBe(project);
                expect(message.getKey()).toBe(key);
                expect(message.getValue()).toBe('[' + key + ']');
                done();
            });
        });

        it('should use the value if specified', function (done) {
            i18nService.addKey(project, key, value).then(function (message) {
                expect(message.getProject()).toBe(project);
                expect(message.getKey()).toBe(key);
                expect(message.getValue()).toBe(value);
                done();
            });
        });

        it('should be added in the ALL-ALL bundle', function (done) {
            i18nService.addKey(project, key, value).then(function (message) {
                expect(message.getLanguage()).toBe(languageCode.ALL);
                expect(message.getCountry()).toBe(countryCode.ALL);
                done();
            });
        });

        it('should not be able to add the same key twice for the same project', function (done) {
            i18nService.addKey(project, key).then(function () {
                i18nService.addKey(project, key).fail(function (err) {
                    expect(err).toEqual(new KeyExistForProjectError());
                    done();
                });
            });
        });

        it('should be able to add the same key for different project', function (done) {
            i18nService.addKey('project1', key).then(function () {
                i18nService.addKey('project2', key).then(function (message) {
                    expect(message).toBeDefined();
                    done();
                });
            });
        });
    });

    describe('addLocale', function () {
        it('should a specific language be linked to the default', function (done) {
            i18nService.addKey(project, key, value).then(function (messageAllAll) {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function (messageFrAll) {
                    expect(messageAllAll.getValue()).toBe(messageFrAll.getValue());
                    done();
                });
            });
        });

        it('should not be able to add a locale for an unexisting key', function (done) {
            i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).fail(function (err) {
                expect(err).toEqual(new KeyNotFoundError());
                done();
            });
        });

        it('should not be possible to add a specific language twice for a specific key', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function () {
                    i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).fail(function (err) {
                        expect(err).toEqual(new KeyExistForLocaleError());
                        done();
                    });
                });
            });
        });

        it('should be possible to add a country if the language exist', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function (messageFrAll) {
                    i18nService.addLocale(project, key, languageCode.FR, countryCode.CA).then(function (messageFrCa) {
                        expect(messageFrAll.getValue()).toBe(messageFrCa.getValue());
                        done();
                    });
                });
            });
        });

        it('should link all related messages together', function (done) {
            i18nService.addKey(project, key, value).then(function (messageAllAll) {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function (messageFrAll) {
                    i18nService.addLocale(project, key, languageCode.FR, countryCode.CA).then(function (messageFrCa) {
                        expect(messageAllAll.hasParent()).toBe(false);
                        expect(messageFrAll.getParentMessage().getId()).toEqual(messageAllAll.getId());
                        expect(messageFrCa.getParentMessage().getId()).toEqual(messageFrAll.getId());
                        expect(messageFrCa.getParentMessage().getParentMessage().getId()).toEqual(messageAllAll.getId());
                        done();
                    });
                });
            });
        });

        it('should not be possible to add a country for a language twice', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function () {
                    i18nService.addLocale(project, key, languageCode.FR, countryCode.CA).then(function () {
                        i18nService.addLocale(project, key, languageCode.FR, countryCode.CA).fail(function (err) {
                            expect(err).toEqual(new KeyExistForLocaleError());
                            done();
                        });
                    });
                });
            });
        });

        it('should not be possible to add a country if the language does not exist', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.CA).fail(function (err) {
                    expect(err).toEqual(new KeyForLanguageIsMissingError());
                    done();
                });
            });
        });
    });

    describe('findMessages', function () {
        it('should be able to find a key that was added', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.findMessages(project, key, languageCode.ALL, countryCode.ALL).then(function (messages) {
                    expect(messages.length).toBe(1);
                    expect(messages[0].getProject()).toBe(project);
                    expect(messages[0].getKey()).toBe(key);
                    expect(messages[0].getValue()).toBe(value);
                    expect(messages[0].getLanguage()).toBe(languageCode.ALL);
                    expect(messages[0].getCountry()).toBe(countryCode.ALL);
                    expect(messages[0].hasParent()).toBe(false);
                    done();
                });
            });
        });

        it('should trigger an error when no message was found', function (done) {
            i18nService.findMessages(project, key, languageCode.ALL, countryCode.ALL).fail(function (err) {
                expect(err).toEqual(new MessageNotFoundError());
                done();
            });
        });

        it('should be able to find a locale that was added', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.addLocale(project, key, languageCode.FR, languageCode.ALL).then(function () {
                    i18nService.findMessages(project, key, languageCode.FR, countryCode.ALL).then(function (messages) {
                        expect(messages.length).toBe(1);
                        expect(messages[0].getProject()).toBe(project);
                        expect(messages[0].getKey()).toBe(key);
                        expect(messages[0].getValue()).toBe(value);
                        expect(messages[0].getLanguage()).toBe(languageCode.FR);
                        expect(messages[0].getCountry()).toBe(countryCode.ALL);
                        expect(messages[0].hasParent()).toBe(true);
                        expect(messages[0].getParentMessage().getLanguage()).toBe(languageCode.ALL);
                        expect(messages[0].getParentMessage().getCountry()).toBe(countryCode.ALL);
                        done();
                    });
                });
            });
        });

        it('should be able to find a specific language and country that was added', function (done) {
            i18nService.addKey(project, key, value).then(function () {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function () {
                    var promise1 = i18nService.addLocale(project, key, languageCode.FR, countryCode.CA);
                    var promise2 = i18nService.addLocale(project, key, languageCode.FR, countryCode.FR);

                    Q.all([promise1, promise2]).then(function () {
                        var promise3 = i18nService.findMessages(project, key, languageCode.FR, countryCode.CA);
                        var promise4 = i18nService.findMessages(project, key, languageCode.FR, countryCode.FR);

                        Q.all([promise3, promise4]).then(function (messages) {
                            var messageFrCa = messages[0][0],
                                messageFrFr = messages[1][0];

                            expect(messageFrCa.hasParent()).toBe(true);
                            expect(messageFrCa.getParentMessage().getLanguage()).toBe(languageCode.FR);
                            expect(messageFrCa.getParentMessage().getCountry()).toBe(countryCode.ALL);

                            expect(messageFrFr.hasParent()).toBe(true);
                            expect(messageFrFr.getParentMessage().getLanguage()).toBe(languageCode.FR);
                            expect(messageFrFr.getParentMessage().getCountry()).toBe(countryCode.ALL);

                            expect(messageFrCa.getParentMessage().getId()).toEqual(messageFrCa.getParentMessage().getId());
                            done();
                        });
                    });
                });
            });
        });
    });

    describe('updateMessage', function () {
        it('should update the value', function (done) {
            i18nService.addKey(project, key).then(function (message) {
                message.setValue('newValue');
                i18nService.updateMessage(message).then(function (messageUpdated) {
                    expect(messageUpdated.getValue()).toBe('newValue');
                    done();
                });
            });
        });

        it('should keep the data linked between parent and children', function (done) {
            i18nService.addKey(project, key).then(function (messageAllAll) {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function () {
                    messageAllAll.setValue('newValue');
                    i18nService.updateMessage(messageAllAll).then(function () {
                        i18nService.findMessages(project, key, languageCode.FR, countryCode.ALL).then(function (messagesFrAll) {
                            expect(messagesFrAll[0].getValue()).toBe('newValue');
                            done();
                        });
                    });
                });
            });
        });

        it('should seperate the link between parent and children when data is set in child', function (done) {
            i18nService.addKey(project, key, value).then(function() {
                i18nService.addLocale(project, key, languageCode.FR, countryCode.ALL).then(function(messageFrAll) {
                    i18nService.addLocale(project, key, languageCode.FR, countryCode.CA).then(function() {
                        messageFrAll.setValue('newValue');
                        i18nService.updateMessage(messageFrAll).then(function() {
                            var promise1 = i18nService.findMessages(project, key, languageCode.ALL, countryCode.ALL);
                            var promise2 = i18nService.findMessages(project, key, languageCode.FR, countryCode.ALL);
                            var promise3 = i18nService.findMessages(project, key, languageCode.FR, countryCode.CA);
                            
                            Q.all([promise1, promise2, promise3]).then(function(messages) {
                                var messageAllAll = messages[0][0];
                                var messageFrAllUpdated = messages[1][0];
                                var messageFrCaUpdated = messages[2][0];
                                
                                expect(messageAllAll.getValue()).toBe(value);
                                expect(messageFrAllUpdated.getValue()).toBe('newValue');
                                expect(messageFrCaUpdated.getValue()).toBe('newValue');
                                done();
                            });
                        });
                    });
                });
            });
        });
    });

});